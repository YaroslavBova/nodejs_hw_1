const fsPromise = require("fs/promises");
const path = require("path");

const passwords = require("../passwords.json");

const createFile = async (req, res) => {
  const { filename, content, password } = req.body;

  try {
    if (password && password !== "") {
      passwords[filename] = password;
      await fsPromise.writeFile("./passwords.json", JSON.stringify(passwords));
    }
    await fsPromise.writeFile(`./files/${filename}`, content, "utf-8");
    res.status(200).json({
      message: "File created successfully"
    });
  } catch (error) {
    res.status(500).json({
      message: "Server error"
    });
  }
};

const getFiles = async (req, res) => {
  try {
    const filesList = await fsPromise.readdir(`./files`);
    res.status(200).json({
      message: "Success",
      files: filesList
    });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

const getFile = async (req, res) => {
  const filename = req.params.filename;
  const ext = path.parse(req.url).ext;

  try {
    const content = await fsPromise.readFile(`./files/${filename}`, "utf-8");
    const createDate = (await fsPromise.stat(`./files/${filename}`)).birthtime;

    res.status(200).json({
      message: "Success",
      filename: filename,
      content: content,
      extension: `${ext.slice(1)}`,
      uploadedDate: createDate
    });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

const deleteFile = async (req, res) => {
  const filename = req.params.filename;

  try {
    const passwordsList = JSON.parse(
      await fsPromise.readFile("./passwords.json", "utf-8")
    );

    await fsPromise.unlink(`./files/${filename}`);

    delete passwordsList[filename];
    await fsPromise.writeFile(
      "./passwords.json",
      JSON.stringify(passwordsList)
    );

    res
      .status(200)
      .json({ message: `File ${filename} has been deleted successfully` });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

const putFile = async (req, res) => {
  const filename = req.params.filename;
  const { content } = req.body;

  try {
    await fsPromise.writeFile(`./files/${filename}`, content, "utf-8");
    res.status(200).json({
      message: `File ${filename} has been changed successfully`
    });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  putFile,
  deleteFile
};
