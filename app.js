const express = require(`express`);
const morgan = require(`morgan`);
const PORT = process.env.PORT ?? 8080;
const router = require(`./routers/router`);

const app = express();

app.use(express.json());

app.use(morgan("tiny"));

app.use("/", router);
app.use("/", express.static("files"));

app.use((req, res, next) => {
  res.status(400).json({ message: `Client error` });
});

app.listen(PORT, () => {
  console.log(`Server has been started on ${PORT}`);
});
