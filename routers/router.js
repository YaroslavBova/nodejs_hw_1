const express = require("express");
const router = express.Router();
const {
  checkGetFileRequest,
  checkCreateFileRequest
} = require("../middlewares/middleware");
const {
  createFile,
  getFiles,
  getFile,
  putFile,
  deleteFile
} = require("../controllers/controllers");

router.get("/api/files", getFiles);
router.post("/api/files", checkCreateFileRequest, createFile);
router.get("/api/files/:filename", checkGetFileRequest, getFile);
router.put("/api/files/:filename", checkGetFileRequest, putFile);
router.delete("/api/files/:filename", checkGetFileRequest, deleteFile);

module.exports = router;
