const fs = require("fs");
const fsPromise = require("fs/promises");

const path = require("path");
const passwords = require("../passwords.json");

const checkExtention = (fileExt) => {
  return [".js", ".json", ".log", ".txt", ".xml", ".yaml"].includes(fileExt);
};

const isDirExist = (pathName) => {
  const dir = path.parse(pathName).base;
  if (fs.existsSync(dir)) {
    return true;
  }
  fs.mkdirSync(dir);
};

const checkGetFileRequest = async (req, res, next) => {
  const filename = req.params.filename;
  const filesList = await fsPromise.readdir(`./files`);

  if (!filesList.includes(filename)) {
    return res.status(400).json({
      message: `No file with '${filename}' filename found`
    });
  }

  const password = req.body.password;

  if (filename in passwords && password !== passwords[filename]) {
    return res.status(400).json({
      message: `Type correct password`
    });
  }

  next();
};

const checkCreateFileRequest = async (req, res, next) => {
  const { filename, content, password } = req.body;
  await isDirExist(req.url);
  const filesList = await fsPromise.readdir("./files");

  if (!filename || !content) {
    const param = filename ? "content" : "filename";
    return res
      .status(400)
      .json({ message: `Please specify '${param}' parameter` });
  }

  const fileExt = filename.slice(filename.lastIndexOf("."));

  if (!checkExtention(fileExt)) {
    return res.status(400).json({ message: `Not supported file extention` });
  }

  if (filesList.includes(filename)) {
    return res
      .status(400)
      .json({ message: `Cannot create file! ${filename} already exists` });
  }

  if (password === "") {
    return res.status(400).json({ message: `Password cannot be empty` });
  }

  next();
};

module.exports = {
  checkGetFileRequest,
  checkCreateFileRequest
};
